<div id="instructions">
	<p><strong>Instructions</strong></p>
	<p>
		Click any window to open and to add an image acting as a preview or message.
	</p>
	<p>
		To link the window to a post, use the drop down menu to the bottom right of the controls to apply to open window. 
		Please note only private posts are made available for this.
	</p>
<p><strong>Shortcode:</strong></p>
<pre>[advent_calendar scramble="true" keep_open="true"]</pre>
<p>Simply paste the above code into the page where you want the calendar to appear. Replace with <strong>"false"</strong> where you either 
don&rsquo;t wish to have the windows arranged randomly, or the previous days&rsquo; windows left opened.</p>
</div>
<div id="calendar_controls">
<form name="adv_calendar_form" id="adv_calendar_form">
<label for="cwidth">Calendar width: </label><input name="cwidth" id="cwidth" type="number" required="required" value="1030" size="4" data-css="#canvas,width" onkeyup="this.click()" onblur="this.click()"/>
<label for="cheight">Calendar height: </label><input name="cheight" id="cheight" type="number" required="required" value="690" size="4" data-css="#canvas,height" onkeyup="this.click()" onblur="this.click()" />
<label for="columns">Columns:</label><input name="columns" id="columns" type="number" value="5" size="1" onkeyup="makeCalendar()" onclick="makeCalendar()" onblue="makeCalendar()"/>
<hr />
<label for="bsize">Window size: </label><input name="bsize" id="bsize" type="number" required="required" value="160" size="4" data-run="makeCalendar" onkeyup="this.click()" onblur="this.click()" />
<label for="space">Window spacing: </label><input name="space" id="space" type="number" required="required" value="11" size="4" data-run="makeCalendar" onkeyup="this.click()" onblur="this.click()" />
<label for="vpos">Vertical position: </label><input name="vpos" id="vpos" type="number" required="required" value="11" size="4" data-run="makeCalendar" onkeyup="this.click()" onblur="this.click()" />
<hr />
<label for="image">Calendar picture: </label><input name="calendar_image" id="calendar_image" type="url" required="required" value="https://s3-eu-west-1.amazonaws.com/corporatewebspace/assets/images/media/advent/advent4.png" size="128" data-css="#canvas,background-image" style="width:582px !important" onfocus="this.select()" onchange="this.click()" onblur="this.click()"/>
<hr />
<label for="door_text">Inside door text</label><input type="text" name="door_text" id="door_text" value="" onkeyup="makeCalendar()" style="width:582px !important" />
<hr />
<label for="dcolor">Inside door colour</label><input type="color" name="dcolor" id="dcolor" onchange="$('.info-back').css('background-color',this.value);$('#info_back').val(this.value)"/>
<label for="bcolor">Back colour</label><input type="color" name="bcolor" id="bcolor" onchange="$('.info-wrap').css('background-color',this.value);$('#info_wrap').val(this.value)"/>
<label for="dsize">Date size</label><input type="number" value="65" name="dsize" id="dsize" onchange="$('.info-front').css('font-size',this.value+'px')"/>
<hr />
<input type="hidden" name="action" id="action" value="save_calendar" />
<input type="hidden" name="doors" id="doors" value="" />
<input type="hidden" name="links" id="links" value="" />
<input type="hidden" name="info_back" id="info_back" value="" />
<input type="hidden" name="info_wrap" id="info_wrap" value="" />
<hr />
<label for="calendar_mode" style="width:100%;border:0;background:transparent;text-align:left;" onclick="makeCalendar()">
<input type="checkbox" name="calendar_mode" id="calendar_mode" value="12" />12 day calendar</label>
<div style="clear:both"></div>
<input type="button" value="Save" onclick="saveCalendar()"/>
<input type="button" value="Restore" onclick="if(confirm('This will restore the calendar to the point of last save. Sure?'))loadCalendar()"/>
<input type="button" value="Import" id="import-file"/>
<input type="button" value="Export" onclick="exportCalendar()"/>
<input type="button" value="Raw CSS edit" onclick="$('#css_wrapper').fadeIn(100,function(){$('#css').focus().scrollTop(0);});"/>

<select id="pages" name="pages" onchange="link[clickeddoor]=this.options[this.selectedIndex].value;$('#links').val(link.toString());">
	<option>Link to&#133;</option>
</select>	
<hr />
<div id="css_wrapper" style="display:none">
<textarea name="css" id="css" rows="30" cols="50" style="width:100%;height:92%"><?php
echo(htmlentities(stripslashes(file_get_contents(plugin_dir_path(__FILE__).'adv_calendar_theme.css'))));
?></textarea>
<input type="button" onclick="$('#css_wrapper').fadeOut()" value="Close" />
<input type="button" onclick="$('#css_wrapper').fadeOut();saveCalendar();setTimeout('window.location.reload()',1000);" value="Save &amp; close" />
</div>
</form>
</div>
<div id="canvas"></div>

<div id="loader" style="position:absolute;top:0;left:0;width:100%;height:100%;display:none">
<div class="loader"></div></div>


<script>
$=jQuery.noConflict();
/* Just some default settings. */
var bsize = 160; // size of window
var space = 11;  // space between windows
var itemx = 0,itemy = 0;
var vpos  = 160;
var cols  = 6;
var ctx   = '';
var door  = new Array();
var link  = new Array();
var xtick = 0,ytick = 0;
var borderwidth = 1;
var clickeddoor = 0;
String.prototype.stripSlashes = function(){
	return this.replace(/\\(.)/mg, "$1");
}
function testDoors(){
	$("#pages :selected").prop('selected', false);
	$('#pages option').each(function(){
		if ($(this).val() == link[clickeddoor])$(this).attr('selected',true);
		console.log(link[clickeddoor]+' is not the same as '+$(this).val());
	});
}
function makeCalendar(){
	var a = 0;
	var cls = parseInt($('#columns').val());
	var out = '';
	var stt = 1;
	bsize = parseInt($('#bsize').val());
	space = parseInt($('#space').val());
	vpos  = parseInt($('#vpos').val());
	if ($('#doors').val().length>0)door = $('#doors').val().split(',');
	if ($('#calendar_mode')[0].checked){
		stt = 13;
	}
	link = $('#links').val().split(',');
	for (var i=stt;i<=24;i++){
		out+= '		<div id="door_'+i+'" class="item" tabindex="'+i+'" style="margin:'+space+'px;width:'+(bsize-space)+'px;height:'+(bsize-space)+'px">';
		out+= '				<div class="info-wrap" style="width:100%;height:100%;background-color:'+$('#info_wrap').val()+'">';
		out+= '                          	<span onclick="$(\'#upload-button\').click()" class="greeting" id="greeting'+i+'" style="';
		if (door[i]==undefined || door[i] == null || door[i] == ''){
			out+= '                          	">+Add a background image for this window';
		}
		else 	out+= '					background-image:url('+door[i]+')">';
		
		out+= '                           	</span>';
		out+= '					<div class="info" style="width:100%;height:100%">';
		out+= '							<div class="info-front stitched_element" style="line-height:'+vpos+'px"><span>'+i+'</span></div>';
		out+= '							<div class="info-back" style="background-color:'+$('#info_back').val()+'"><div class="door-text" style="">'+$('#door_text').val()+'</div></div>';
		out+= '					</div>';
		out+= '				</div>';
		out+= '		</div>';
		a++;
		if (a==cls){
				out+= '<div style="clear:both"></div>';
				a = 0;
		}
	}
	out+= '<div style="clear:both"></div>';
	$('#canvas').html(out).css('background-image','url('+$('#calendar_image').val()+')');
	$('.item').click(function(){clickeddoor=$(this).attr('tabindex');testDoors();});
	
	itemx = 0;
	itemy = 0;
	xtick = 0;
	ytick = 0;
	bsize = parseInt($('#bsize').val());
	space = parseInt($('#space').val());
	$('.info-front').each(function(){
	$(this).css({
	'font-size':$('#dsize').val()+'px',
	'background-image':'url('+$('#calendar_image').val()+')',
	'background-position-x':(itemx-space-borderwidth)+'px',
	'background-position-y':(itemy-space-borderwidth)+'px'});
	itemx-= bsize+space;
		xtick+= 1;
		if (xtick>=cls){
				xtick = 0;
				ytick+= 1;
				itemy-= bsize+space;
				itemx = 0;
		}
	});
	$('#calendar_controls').fadeIn(1000,function(){$('#canvas').fadeIn('slow');});
}
	
$(document).ready(function(){
	console.log('Loading pages...');
	getDoors();
	console.log('Loading calendar...');
	loadCalendar();
	$('input').click(function(){
		if($(this).data('css') != undefined){
			var spl = $(this).data('css').split(',');
			if ($(this)[0].type == 'url'){
				$(spl[0]).css(spl[1],'url('+$(this).val()+')');
				makeCalendar(cols);
				$('.info-front').css('background-image','url('+$(this).val()+')');
			}
			else $(spl[0]).css(spl[1],$(this).val());
			console.log(spl[1]+':'+$(this).val());
			return this;
		}
		if($(this).data('run') != undefined){
			if ($(this).data('run') == 'makeCalendar'){
				makeCalendar(cols);
			}
		}
	});
	/*Media*/
	var mediaUploader;
	$('#upload-button').click(function(e) {
		ctx = $(this).attr('id');
		e.preventDefault();
		// If the uploader object has already been created, reopen the dialog
		if (mediaUploader) {
			mediaUploader.open();
		}
		return;
	});
	$('#import-file').click(function(e) {
		ctx = $(this).attr('id');
		e.preventDefault();
		// If the uploader object has already been created, reopen the dialog
		if (mediaUploader) {
			mediaUploader.open();
		}
		return;
	});
	$('#calendar_image').click(function(e) {
		ctx = $(this).attr('id');
		e.preventDefault();
		// If the uploader object has already been created, reopen the dialog
		if (mediaUploader) {
			mediaUploader.open();
		}
		return;
	});
	// Extend the wp.media object
	mediaUploader = wp.media.frames.file_frame = wp.media({
		title: 'Choose Image',
		button: {
		text: 'Choose Image'
		}, multiple: false });

	// When a file is selected, grab the URL and set it as the text field's value
	mediaUploader.on('select', function() {
		attachment = mediaUploader.state().get('selection').first().toJSON();
		if(ctx=='import-file'){
			console.log('Attempting to restore from file: '+attachment.url);
			importCalendar(attachment.url);
		}
		else{
			if(ctx=='')ctx = 'image-url';
			$('#'+ctx).val(attachment.url);
			$('#greeting'+clickeddoor).css('background-image','url('+attachment.url+')');
			setTimeout("$('#door_'+clickeddoor)[0].focus()",200);
			door[clickeddoor] = attachment.url;
			$('#doors').val(door.toString());
			makeCalendar();
		}
	});
	// Open the uploader dialog
	//mediaUploader.open();
});

</script>
<form method="post" style="display:none">
  <input id="image-url" type="text" name="image" />
  <input id="upload-button" type="button" class="button" value="Upload Image" />
  <input type="submit" value="Submit" />
</form>