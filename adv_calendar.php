<?php
/*
Plugin Name: Customisable Advent Calendar
Plugin URI:  https://frontendstudios.co.uk/advent
Description: This plugin allows you to create an advent calendar with small web pages hidden behind a door for each day.
Version:     1.0
Author:      Hakun Kamminga <hakun.kamminga@gmaill.com>
Author URI:  
License:     GPL2
License URI: https://www.gradwell.com/licence/plugins
Domain Path: /languages
Text Domain: my-toolset
*/
define( 'ADV_CALENDAR_VERSION','1.0' );
defined( 'ABSPATH' ) or die( 'Plugin failure. Did you install it correctly?' );
wp_register_style('adv-calendar', plugins_url('adv_calendar.css', __FILE__));
wp_register_style('adv-calendar_theme', plugins_url('adv_calendar_theme.css', __FILE__));
wp_enqueue_style('adv-calendar');  
wp_enqueue_style('adv-calendar_theme');  
wp_enqueue_style('thickbox'); // call to media files in wp
wp_enqueue_script('thickbox');
wp_enqueue_script('media-upload');
add_action( 'wp_head', 'add_ajaxurl_cdata');
add_action('admin_menu', 'adv_calendar_menu');
add_action( 'wp_ajax_adv_ui', 'adv_ui' );
add_action( 'wp_ajax_nopriv_adv_ui', 'adv_ui' );
add_action( 'wp_ajax_save_calendar', 'adv_action_callback' );
add_action( 'wp_ajax_load_calendar', 'adv_action_callback' );
add_action( 'wp_ajax_get_doors', 'adv_action_callback' );
add_action( 'wp_ajax_open_window','adv_action_callback ');
add_action( 'wp_ajax_import_calendar','adv_action_callback');
add_action( 'wp_ajax_export_calendar','adv_action_callback');
add_action('admin_enqueue_scripts', 'my_media_lib_uploader_enqueue');
add_shortcode( 'advent_calendar', 'make_calendar' );
require_once(plugin_dir_path(__FILE__).'adv_calendar_functions.php');