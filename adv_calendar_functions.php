<?php
/**
 * Plugin functions.
 * These are deliberately kept away from wp's own functions.php
 * for better visibility / portability.
 * Note to self: Use boilerplate in future for quicker build.
 */
function add_ajaxurl_cdata(){ 
	echo('    <script type="text/javascript"> //<![CDATA[
        ajaxurl = "'.admin_url('admin-ajax.php').'"; 
	//]]> </script>');
}
function adv_ui(){
	global $wpdb;
	if (!empty($_POST['request'])){
		switch($_POST['request']){
			case "open_door":
			$tod = intval(date('j',time()));
			$day = intval($_POST['day']);
			if ($day>$tod)echo('<img src="http://blackphobos.com/wp-content/uploads/2015/11/Disapproving-Santa.jpg" style="display:inline;text-align:center" /><h1>Tut-tut!!</h1><h2><br/>It is too soon to open this window, for now.</h2>');
			else{
				$res = json_decode(get_option('adv_calendar_general'));
				$drs = explode(',',$res->links);
				$a = get_post($drs[$day]);
				echo wpautop(do_shortcode($a->post_content));
			}
		}
	}
	wp_die();
}
function adv_calendar_menu(){
	add_options_page( 'Manage Calendars', 'Advent Calendar', 'manage_options', 'adv_calendar', 'adv_calendar_options' );
}
function adv_calendar_options() {
	/**
	 * Primary calendar constructor. Do perms check here because without
	 * loading the editor, nothing can happen.
	*/
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo '<div class="wrap"><h2>Customisable Advent Calendar v'.ADV_CALENDAR_VERSION.'</h2>';
	require_once($dir.'adv_calendar_editor.php');
	echo '</div>';
	add_action( 'admin_footer', 'adv_action_javascript' ); 
}
function adv_action_javascript() { 
	echo("
	<script type=\"text/javascript\">
	function saveCalendar(){
		$('#loader').fadeIn(400);
		$('#doors').val(door.toString());
		jQuery.post(ajaxurl, $('#adv_calendar_form').serialize(), function(response) {
			setTimeout(\"$('#loader').fadeOut(400)\",500);
		});
	}
	function importCalendar(file_url){
		var data = {
			'action':'import_calendar',
			'file_url':file_url
		};
		jQuery.post(ajaxurl, data, function(response) {
			alert(response);
			window.location.reload();
		});
	}
	function exportCalendar(){
		var data = {
			'action':'export_calendar'
		};
		jQuery.post(ajaxurl, data, function(response) {
			$('#instructions').css({'text-align':'center','position':'absolute','left':'30%','width':'40%'}).html('<h1>Export ready.</h1><p><a href=\"'+response+'\">Download</a><p>Right-click the link above and click Save link as&#133;</p><p>This file is plaintext and does not contain any code.</p><input type=\"button\" value=\"Close\" onclick=\"$(&quot;#instructions&quot;).fadeOut()\"/> ');
		});
	}
	function getDoors(){
		var data = {
			'action':'get_doors'
		};
		jQuery.post(ajaxurl, data, function(response) {
			console.log('Got this from the server: ' + response);
			var d = eval('('+response+')');
			var o = '';
			if (d.length>0){
				o+= '<option value=\"-1\">Link to&#133;</option>';
				for(var i=0;i<d.length;i++){
					if (d[i].post_title == '')d[i].post_title = 'Please give this page / post a name :(';
					o+= '<option value=\"'+d[i].ID+'\">'+d[i].post_title+'</option>';
				}
				$('#pages').html(o);
			}
			else alert('Sorry, I could not find any pages or posts to link to that are private!');
		});
	}
	function loadCalendar(){
		$('#loader').fadeIn(400);
		var data = {
			'action': 'load_calendar'
		};
		jQuery.post(ajaxurl, data, function(response) {
			if (response.length>0){
				var d = eval('('+response+')');
				for(var i in d){
					jQuery('#'+i).val(d[i].stripSlashes());
				}
				door = $('#doors').val().split(',');
				link = $('#links').val().split(',');
				$('#canvas').css('width',$('#cwidth').val()+'px');
				$('#bcolor').css('background-color',$('#info-wrap').val());
				$('.info-wrap').css('background-color',$('#info-wrap').val());
				$('#dcolor').css('background-color',$('#info-back').val());
				$('.info-back').css('background-color',$('#info-back').val());
			}
			setTimeout(\"$('#loader').fadeOut(400)\",500);
			makeCalendar();
		});
	}
	</script>");
}

function adv_action_callback() {
	global $wpdb;
	switch($_POST['action']){
		case "import_calendar":
		$test = json_decode(base64_decode(file_get_contents($_POST['file_url'])));
		echo("received file". $_POST['file_url']);
		if (isset($test->cwidth) && isset($test->cheight) && isset($test->bsize) && isset($test->dsize)){
			// Passes initial test. Sanitize:
			foreach($test as $obj=>$val){
				$test->$obj = strip_tags($val);
				$arr[$obj] = strip_tags($val);
			}
			update_option("adv_calendar_general",json_encode($arr));
			file_put_contents(plugin_dir_path(__FILE__).'adv_calendar_theme.css',strip_tags(stripslashes($arr['css'])));
		}
		echo("\nColumns: ".$test->columns);
		break;
		case "export_calendar":
		$res = get_option('adv_calendar_general');
		file_put_contents(plugin_dir_path(__FILE__).'custom_advent_calendar.txt',base64_encode($res));
		echo(plugins_url( 'custom_advent_calendar.txt', __FILE__ ));
		break;
		case "save_calendar":
		update_option("adv_calendar_general",json_encode($_POST));
		file_put_contents(plugin_dir_path(__FILE__).'adv_calendar_theme.css',strip_tags(stripslashes($_POST['css'])));
		echo('Saved calendar.');
		break;
		case "load_calendar":
		$res = get_option('adv_calendar_general');
		echo($res);
		break;
		case "delete_calendar":
		break;
		case "update_fields":
		break;	
		case "get_doors":
		$results = $wpdb->get_results( 'SELECT ID,post_title FROM wp_posts WHERE post_status = "private" and post_type = "post" order by post_title', ARRAY_A);
		echo (json_encode($results));
		break;
	}
	wp_die(); // this is required to terminate immediately and return a proper response
}
function my_media_lib_uploader_enqueue() {
	wp_enqueue_media();
}
function make_calendar( $atts ) {
	$a = shortcode_atts( array(
	'scramble' => false,
	'keep_open' =>true,
	), $atts );
	
	$arr = json_decode(get_option('adv_calendar_general'));
	$doors = explode(",",$arr->doors);
	$links = explode(",",$arr->links);
	$out = '<div id="window_large" style="display:none"><div id="window_message"><div id="window_message_post"></div></div>';
	$out.= '<div id="close" onclick="jQuery(\'#window_large\').fadeOut()">Close</div></div>';
	if ($atts['debug'] == 'true')$out.= "<pre>".print_r($atts,1)."</pre>";
	$out.= '<div id="canvas" style="background-image:url('.$arr->calendar_image.');width:'.$arr->cwidth.'px;height:'.$arr->cheight.'px">';
	$itemx = 0;
	$itemy = 0;
	$a = 0;
	$d = intval(date('j',time()));
	$window = range(1,24);
	if ($atts["scramble"] == 'true')shuffle($window);
	foreach($window as $i){
		$out.= '	<div id="door_'.$i.'" class="item" tabindex="'.$i.'" style="margin:'.$arr->space.'px;width:'.($arr->bsize-$arr->space).'px;height:'.($arr->bsize-$arr->space).'px">';
		$out.= '	<div class="info-wrap" style="width:100%;height:100%;background-color:'.$arr->info_wrap.'">';
		$out.= '                  <span onclick="open_window('.$i.',this)" class="greeting" id="greeting'.$i.'" style="';
		if (empty($doors[$i])){
			$out.= '">';
		}
		else 	$out.= 'background-image:url('.$doors[$i].')">';
		$style = 'background-image:url('.$arr->calendar_image.');';
		$style.= 'background-position:'.($itemx-$arr->space-1).'px';
		$style.= ' '.($itemy-$arr->space-1).'px;';
		$style.= 'line-height:'.$arr->vpos.'px;';
		$style.= 'font-size:'.$arr->dsize.'px;';
		$itemx-= ($arr->bsize+$arr->space);
		$out.= '                          </span>';
		if ($i>=$d || $atts["keep_open"]=="false"){

			$out.= '			<div class="info" style="width:100%;height:100%">';
			$out.= '					<div class="info-front stitched_element" style="'.$style.'"><span>'.$i.'</span></div>';
			$out.= '					<div class="info-back" style="background-color:'.$arr->info_back.'">';
			$out.= '				<div class="door-text" style="">'.$arr->door_text.'</div></div>';
			$out.= '			</div>';
		}
		$out.= '		</div>';
		$out.= '		</div>';
		$a++;
		if ($a>=$arr->columns){
				$itemy-= ($arr->bsize+$arr->space);
				$itemx = 0;
				$out.= '<div style="clear:both"></div>';
				$a = 0;
		}
	}
	$out.= '</div><script>jQuery("#canvas").delay(550).fadeIn(900);
	/**
	 * detect IE
	 * returns version of IE or false, if browser is not Internet Explorer
	 */
	function detectIE() {
	    var ua = window.navigator.userAgent;	
	    var msie = ua.indexOf("MSIE ");
	    if (msie > 0) {
	        // IE 10 or older => return version number
	        return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
	    }
	    var trident = ua.indexOf("Trident/");
	    if (trident > 0) {
	        // IE 11 => return version number
	        var rv = ua.indexOf("rv:");
	        return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
	    }
	    var edge = ua.indexOf("Edge/");
	    if (edge > 0) {
	       // IE 12 (aka Edge) => return version number
	       return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
	    }
	    // other browser
	    return false;
	}

	function open_window(day,e){
		jQuery(e).animate({\'background-size\':\'220%\'},300);
		var data = {
			\'action\': \'adv_ui\',
			\'request\': \'open_door\',
			\'day\': day
		};
		jQuery.post(ajaxurl,data,function(response){
			jQuery(e).animate({\'background-size\':\'50%\'},200);
			jQuery("#window_message_post").html(response);
			jQuery("#window_large").fadeIn(1200);
			jQuery("html, body").animate({scrollTop:0,scrollLeft:0}, "slow");
		});
	}
	</script><style>'.$arr->css.'</style>';

	$out.= "<script>
	var focusIsSupported = (function(){

	    // Create an anchor + some styles including ':focus'.
	    // Focus the anchor, test if style was applied,
	    // if it was then we know ':focus' is supported.
 
	    var ud = 't' + +new Date(),
	        anchor = jQuery('<a id=\"' + ud + '\" href=\"#\"/>').css({top:'-999px',position:'absolute'}).appendTo('body'),
	        style = jQuery('<style>#'+ud+'{font-size:10px;}#'+ud+':focus{font-size:1px !important;}</style>').appendTo('head'),
	        supported = anchor.focus().css('fontSize') !== '10px';
	    anchor.add(style).remove();
	    return supported;
 
	})();</script>";


	//$post = get_post(52);
	//return $post->post_content;
	return $out;
}
